# -*- coding:utf-8 -*-
import os
from random import choice

from common.const import CONST
from common.loggers.logger import log
from common.wrappers.etcd_wrapper import EtcdWrapper
from common.wrappers.mongo_wrapper import MongoWrapper
from common.wrappers.redis_queue import RedisQueue
from common.wrappers.redis_wrapper import RedisWrapper
from monitor.taskqueue_handler import TaskQueueHandler
from service.globals import Globals
from settings.setting import SETTING
from spider.crawler import Task


class _BaseInitializer:
    def __init__(self):
        self.has_been_initializer = False
        self.base_dir = '.'

    @classmethod
    def _init_redis(cls):
        redis_wrapper = RedisWrapper(host=SETTING.REDIS_HOST, port=SETTING.REDIS_PORT, password=SETTING.REDIS_PASSWORD)
        Globals.set_redis_wrapper(redis_wrapper)
        log.info('initialize redis successfully.')

    @classmethod
    def _init_etcd(cls):
        etcd_wrapper = EtcdWrapper(host=os.environ.get('ETCD_SERVICE_SERVICE_HOST', '192.168.0.1'),
                                   port=int(os.environ.get('ETCD_SERVICE_SERVICE_PORT', '22379')))
        etcd_wrapper.connect_etcd_cluster()
        Globals.set_etcd_wrapper(etcd_wrapper)
        log.info('initialize ETCD successfully.')

    def _init_setting(self):
        SETTING.set_base_dir(self.base_dir)
        SETTING.reload_from_local()
        SETTING.reload_from_remote(Globals.get_etcd_wrapper())

        log.info('initialize SETTING successfully.')

    @classmethod
    def _init_mongodb(cls):
        mongo_wrapper = MongoWrapper()
        mongo_wrapper.connect_mongo()
        Globals.set_mongo_wrapper(mongo_wrapper)

        log.info('initialize mongodb successfully.')

    @classmethod
    def _init_task_queue(cls):
        proxy_ip_pool = RedisQueue('proxy_ip_pool', Globals.get_redis_wrapper().get_redis_conn())
        task_queue = RedisQueue('crawl_task', Globals.get_redis_wrapper().get_redis_conn())
        # 清空任务队列
        log.info('start to clear task_queue')
        while not task_queue.is_empty():
            task_queue.get()
        for url in CONST.URL_LIST:
            raw_proxy = proxy_ip_pool.get()
            # while raw_proxy is None:
            #     raw_proxy = proxy_ip_pool.get()
            #     time.sleep(1)

            task = Task(url, raw_proxy, choice(CONST.USER_AGENT_LIST))
            task_queue.put(task.__dict__)

        log.info('initialize task_queue successfully.')

    def initialize(self):
        log.info('try to initialize the base wrappers.')
        if self.has_been_initializer:
            log.info('system management has been initialized yet.')
            return

        # Attention: Don't change the order for initializing component.
        # env --> etcd --> setting --> redis --> stat
        self._init_etcd()
        self._init_setting()
        self._init_redis()
        self._init_mongodb()
        self._init_task_queue()

        TaskQueueHandler.start()
        log.info('base initialize successfully.')
        self.has_been_initializer = True

    def de_initialize(self):
        try:
            log.info('try to de-initialize the system management.')
            if not self.has_been_initializer:
                log.info('base has not been initialized.')
                return
            TaskQueueHandler.stop()
            self.has_been_initializer = False
            log.info('base de initialize successfully.')
        except Exception as err:
            log.info('stop base initializer error {}'.format(err))


BS_INITIALIZER = _BaseInitializer()
