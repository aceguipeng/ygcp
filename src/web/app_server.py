# -*- coding:utf-8 -*-

from flask import Flask
from flask_restful import Api
from tornado.httpserver import HTTPServer
from tornado.ioloop import IOLoop
from tornado.wsgi import WSGIContainer

from api.creepy_info import CreepyInfo
from common.const import CONST
from settings.setting import SETTING

app = Flask(__name__, static_folder=CONST.APP_STATIC_FOLDER, static_url_path=CONST.STATIC_URL)
api = Api(app)

api.add_resource(CreepyInfo, '/ygcp/web/api/v1/creepy_info')


@app.route('/ygcp/web/api/v1/is_alive')
def alive():
    return 'yes'


def run_web_service():
    http_server = HTTPServer(WSGIContainer(app))
    http_server.bind(SETTING.WEB_SERVER_PORT)
    http_server.start(SETTING.TORNADO_PROCESSES)
    loop = IOLoop.instance()
    loop.start()


def stop_web_service():
    loop = IOLoop.instance()
    loop.stop()
