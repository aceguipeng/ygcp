# -*- coding:utf-8 -*-
import threading
import time

from common.loggers.logger import log
from settings.setting import SETTING


class __ProcessMonitor:
    def __init__(self):
        self.is_running = False

    def monitor(self):
        counter = 0
        while self.is_running:
            try:
                time.sleep(SETTING.MONITOR_SLEEP_TIME)
                if counter % SETTING.SETTING_REFRESH_INTERVAL == 0:
                    log.debug('try to syn configure from remote...')
                    SETTING.reload_from_remote()

                counter += 1
            except Exception as e:
                try:
                    log.error('monitor_thread error: {}'.format(str(e)))
                    time.sleep(SETTING.ERROR_WAIT_TIME)
                except Exception as pass_err:
                    log.debug(str(pass_err))

        log.info('monitor thread exit.')

    def start(self):
        log.info('try to start monitor thread')
        monitor_thread = threading.Thread(
            target=self.monitor, name='monitor_thread')
        self.is_running = True
        monitor_thread.start()
        log.info('start monitor thread successfully')

    def stop(self):
        log.info('try to stop monitor thread')
        self.is_running = False
        log.info('stop monitor thread successfully')


ProcessMonitor = __ProcessMonitor()
