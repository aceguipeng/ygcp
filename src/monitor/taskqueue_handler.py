# -*- coding:utf-8 -*-
import threading
import time
import traceback

import os
import requests

from common.const import CONST
from common.loggers.logger import log
from common.wrappers.redis_queue import RedisQueue
from service.globals import Globals
from service.tumblr.tumblr_selenium_service import process_tumblr
from service.twitter.twitter_selenium_service import process_twitter
from service.untils import check_ip_conn, insert_items
from settings.setting import SETTING
from spider.crawler import CrawlerManager


class __TaskQueueHandler(object):
    def __init__(self):
        self.is_running = False
        self.process_spider_queue = None
        self.get_proxy_ip_queue = None
        self.process_item_queue = None
        self.task_queue = None
        self.crawl_mananger = None
        self.clear_queue = None

    def put_proxy_ip(self):
        # 存入有效ip
        ip_proxy_url = SETTING.GET_IP_PROXY_URL
        self.get_proxy_ip_queue = RedisQueue('proxy_ip_pool', Globals.get_redis_wrapper().get_redis_conn())
        while self.is_running:
            try:
                while self.get_proxy_ip_queue.queue_size() < SETTING.IP_POOL_SIZE:
                    log.info('try to get proxy ips from server:{}'.format(ip_proxy_url))
                    resp = requests.get(ip_proxy_url)
                    resp_content = resp.json()
                    log.info('has received the data:{} from server:{}'.format(resp_content, ip_proxy_url))
                    if resp_content and len(resp_content['list']) > 0:
                        proxy_ips = resp_content['list']
                        for ip_dict in proxy_ips:
                            ip = ip_dict['addr']
                            # 校验ip有效性
                            if check_ip_conn(ip):
                                self.get_proxy_ip_queue.put(ip_dict)
                            else:
                                continue
                    time.sleep(10)
                if self.get_proxy_ip_queue.queue_size() >= SETTING.IP_POOL_SIZE:
                    for i in range(0, SETTING.IP_POOL_SIZE):
                        ip_dict = self.get_proxy_ip_queue.get()
                        ip = ip_dict['addr']
                        if check_ip_conn(ip):
                            self.get_proxy_ip_queue.put(ip_dict)
            except Exception:
                log.error(traceback.format_exc())
                time.sleep(SETTING.TASK_ERROR_WAIT_TIME)
                continue

    def process_items(self):
        # 把html处理成对象存入mongdb
        self.process_item_queue = RedisQueue('process_items', Globals.get_redis_wrapper().get_redis_conn())
        while self.is_running:
            if self.process_item_queue.is_empty():
                log.debug('process_html_to_items_queue is empty...')
                time.sleep(SETTING.QUEUE_EMPTY_TIME)
                continue
            while not self.process_item_queue.is_empty():
                task = None
                file_name = None
                try:
                    items = None
                    task = self.process_item_queue.get()

                    if task:
                        task = eval(task)
                        file_name = task.get('file_name')
                        log.info('start to process html:{}'.format(file_name))
                        if task.get('source_web') == 'Twitter':
                            items = process_twitter(file_name)
                        elif task.get('source_web') == 'Tumblr':
                            items = process_tumblr(file_name)
                        log.info('complete process html:{}'.format(file_name))
                        insert_items(items)
                except FileNotFoundError:
                    log.error('the file {} is not found'.format(file_name))
                    continue
                except Exception:
                    log.error(traceback.format_exc())
                    if task:
                        self.process_item_queue.put(task)
                    time.sleep(SETTING.TASK_ERROR_WAIT_TIME)
                    continue

    def clear_cache_file(self):
        self.clear_queue = RedisQueue('clear', Globals.get_redis_wrapper().get_redis_conn())
        while self.is_running:
            if self.clear_queue.is_empty():
                log.debug('clear_queue is empty...')
                time.sleep(SETTING.QUEUE_EMPTY_TIME)
                continue
            size = self.clear_queue.queue_size()
            log.info('start clear cache file,file size is {}'.format(size))
            try:
                for i in range(0, size):
                    current_time = int(round(time.time() * 1000))
                    file_data = eval(self.clear_queue.get())
                    file_name = file_data['file_name']
                    file_timestamp = int(file_data['timestamp'])

                    if (current_time - file_timestamp) // 1000 > SETTING.FILE_CACHE_LIFE:
                        if os.path.exists(file_name):
                            os.remove(file_name)
                    else:
                        self.clear_queue.put(file_data)

                current_size = self.clear_queue.queue_size()
                clear_file_count = size - current_size
                log.info('complete clear cache file,clear {} files,current file size is {}'.format(clear_file_count,
                                                                                                   current_size))
                time.sleep(SETTING.FILE_CACHE_LIFE)
            except Exception:
                log.error(traceback.format_exc())
                time.sleep(SETTING.FILE_CACHE_LIFE)
                continue

    def start(self):
        task_queue = RedisQueue('crawl_task', Globals.get_redis_wrapper().get_redis_conn())

        log.info('try to start queue handlers:crawlManager,process_items,clear_cache_files')
        # get_ip_thread = threading.Thread(target=self.process_spider, name='process_spider')
        self.crawl_mananger = CrawlerManager(task_queue, SETTING.MAX_RETRY_TIME)
        process_thread = threading.Thread(target=self.process_items, name='process_items')
        clear_thread = threading.Thread(target=self.clear_queue, name='clear_cache_files')

        self.is_running = True
        self.crawl_mananger.start()
        process_thread.start()
        clear_thread.start()

    def stop(self):
        log.info('try to stop queue handlers')
        self.is_running = False
        self.crawl_mananger.stop()


TaskQueueHandler = __TaskQueueHandler()
