# -*- coding:utf-8 -*-
import traceback

from common.loggers.logger import log
from initializer.initializer import init, de_init
from web.app_server import run_web_service


def start_all():
    init()
    run_web_service()


if __name__ == '__main__':
    try:
        start_all()
    except Exception as e:
        log.error(traceback.format_exc())
        de_init()
        exit(0)
