# -*- coding:utf-8 -*-
from common.wrappers.etcd_wrapper import EtcdWrapper
from common.wrappers.mongo_wrapper import MongoWrapper
from common.wrappers.redis_wrapper import RedisWrapper


class __Globals:
    __hbase_wrapper = None
    __hdfs_wrapper = None
    __mysql_wrapper = None
    __redis_queue = None
    __redis_wrapper = None
    __etcd_wrapper = None
    __selenium_wrapper = None
    __mongo_wrapper = None

    def __init__(self):
        pass

    def get_redis_wrapper(self) -> RedisWrapper:
        return self.__redis_wrapper

    def set_redis_wrapper(self, redis_wrapper):
        self.__redis_wrapper = redis_wrapper

    def get_etcd_wrapper(self) -> EtcdWrapper:
        return self.__etcd_wrapper

    def set_etcd_wrapper(self, etcd_wrapper):
        self.__etcd_wrapper = etcd_wrapper

    def get_mongo_wrapper(self) -> MongoWrapper:
        return self.__mongo_wrapper

    def set_mongo_wrapper(self, mongo_wrapper):
        self.__mongo_wrapper = mongo_wrapper


Globals = __Globals()
