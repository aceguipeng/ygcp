# -*- coding:utf-8 -*-
import time

import pymongo
from bson import ObjectId

from service.globals import Globals


def select_creepy_info(dict_data, sort, sorted_way):
    params = dict_data.copy()
    for i in ['page_no', 'page_size', 'sorted_way', 'sorted_type']:
        if i in params:
            params.pop(i)
    mongo_db = Globals.get_mongo_wrapper().db
    collection = mongo_db['test_creepy_info']  # TODO 上限前修改为setting

    # 根据时间筛选
    if 'update_time' in params:
        update_time = params['update_time']
        params['update_time'] = {"$regex": update_time}

    # 根据时间区间筛选
    if 'date' in params:
        date = int(params['date'])
        target_date_time = time.localtime(time.time() - date * 24 * 60 * 60)
        target_date = time.strftime("%Y-%m-%d %H:%M:%S", target_date_time)
        params['update_time'] = {"$gt": target_date}
        params.pop('date')
    new_params = {"$and": []}
    for k, v in params.items():
        new_params['$and'].append({k: v})

    all_count = collection.find(new_params).count()
    if all_count <= 0:
        result_list = {
            'result': 'success',
            'creepy_info': [],
            'all_count': 0
        }
        return result_list

    page_no = int(dict_data.get('page_no'))
    page_size = int(dict_data.get('page_size'))
    if page_size is None or page_size <= 0:
        page_size = 1
    page_max = all_count // page_size if all_count % page_size == 0 else all_count // page_size + 1
    if page_no is None or page_no <= 0:
        page_no = 1
    if page_no >= page_max:
        page_no = page_max

    if sorted_way == -1:
        result = list(
            collection.find(new_params).sort(sort, pymongo.DESCENDING).skip((page_no - 1) * page_size).limit(page_size))
    else:
        result = list(
            collection.find(new_params).sort(sort, pymongo.ASCENDING).skip((page_no - 1) * page_size).limit(page_size))

    # 处理id
    def change_id(data):
        data['id'] = str(data['_id'])
        data.pop('_id')
        return data

    result = list(map(change_id, result))
    result_list = {
        'result': 'success',
        'creepy_info': result,
        'all_count': all_count
    }
    return result_list


def select_one_creepy(dict_data):
    if not isinstance(dict_data['id'], ObjectId):
        params = {
            '_id': ObjectId(dict_data['id'])
        }
    else:
        params = {
            '_id': dict_data['id']
        }
    mongo_db = Globals.get_mongo_wrapper().db
    collection = mongo_db['test_creepy_info']  # TODO 上限前修改为setting
    result_obj = collection.find_one(params)
    result = {
        'result': 'success',
        'creepy_info': result_obj
    }
    return result
