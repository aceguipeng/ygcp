# -*- coding:utf-8 -*-
import json

import time
from bs4 import BeautifulSoup, Tag

from common.const import CONST
from service.globals import Globals


def process_twitter(file_path):
    soup = BeautifulSoup(open(file_path, encoding=CONST.UTF_8), 'lxml')
    tweets = soup.select('li[class="js-stream-item stream-item stream-item "]')
    subject = soup.find('h1', class_='SearchNavigation-titleText').string.strip()
    count = 0
    items = []
    for tweet in tweets:
        item = get_twitter_tweet(tweet)
        item['subject'] = subject
        if count > 20:  # TODO 测试用，上线前修改到setting
            break
        old_item = Globals.get_redis_wrapper().get_redis_conn().get(
            '{}:{}'.format('data_tweet_id', item['data_tweet_id']))
        if old_item:
            continue
        else:
            items.append(item)
            Globals.get_redis_wrapper().get_redis_conn().set('{}:{}'.format('data_tweet_id', item['data_tweet_id']),
                                                             json.dumps(item),
                                                             ex=2592000)  # TODO 测试用，上线前修改到setting
            count += 1
    return items


def get_twitter_tweet(tweet: Tag):
    item = {
        'data_tweet_id': tweet['data-item-id'],
        'heat': tweet.find('span', class_='ProfileTweet-actionCountForPresentation').string if tweet.find('span',
                                                                                                          class_='ProfileTweet-actionCountForPresentation') else 0,
        'source_web': 'Twitter',
        'update_time': time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time())),

    }
    content_body = tweet.find('div', class_='js-tweet-text-container')
    if content_body:
        content = content_body.get_text(strip=True).strip()
    else:
        content = None
    item['content'] = content
    img_divs = tweet.find_all('div', class_='AdaptiveMedia-photoContainer js-adaptive-photo ')
    video_tag = tweet.find('video')
    if img_divs:
        compose = ['text', 'photo']
        resource = []
        for img_div in img_divs:
            resource.append({
                'type': 'photo',
                'addr': img_div.find('img')['src']
            })
    elif video_tag:
        compose = ['text', 'video']
        resource = [{
            'type': 'video',
            'addr': video_tag.find('source')['src']
        }]
    else:
        compose = ['text']
        resource = None
    item['resource'] = resource
    item['compose'] = compose
    return item
