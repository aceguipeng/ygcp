# -*- coding:utf-8 -*-
import json
import time

from bs4 import BeautifulSoup, Tag

from common.const import CONST
from service.globals import Globals


def process_tumblr(file_path):
    soup = BeautifulSoup(open(file_path, encoding=CONST.UTF_8), 'lxml')
    articles = soup.find_all('article')
    subject = soup.find('h1', class_='search_term_heading').string
    count = 0
    items = []
    for article in articles:
        item = get_tumblr_article(article)
        item['subject'] = subject
        if count > 20:  # TODO 测试用，上线前修改到setting
            break
        # TODO redis去重
        old_item = Globals.get_redis_wrapper().get_redis_conn().get(
            '{}:{}'.format('data_tumblr_id', item['data_tumblr_id']))
        if old_item:
            continue
        else:
            items.append(item)
            Globals.get_redis_wrapper().get_redis_conn().set('{}:{}'.format('data_tumblr_id', item['data_tumblr_id']),
                                                             json.dumps(item),
                                                             ex=2592000)  # TODO 测试用，上线前修改到setting
            count += 1
    return items


def get_tumblr_article(article: Tag):
    item = {
        'data_tumblr_id': article['data-id'],
        'heat': article.find('span', class_='note_link_current')['data-count'] if
        article.find('span', class_='note_link_current') else 0,
        'source_web': 'Tumblr',
        'update_time': time.strftime('%Y-%m-%d %H:%M:%S', time.localtime(time.time())),
    }
    content_body = article.find('div', class_='post_body')
    if content_body:
        content = content_body.get_text(strip=True).strip()
    else:
        content = None
    item['content'] = content
    img_tag = article.find('img')
    video_tag = article.find('video')
    if img_tag:
        compose = ['text', 'photo']
        resource = [{
            'type': 'photo',
            'addr': img_tag['src']
        }]
    elif video_tag:
        compose = ['text', 'video']
        resource = [{
            'type': 'video',
            'addr': video_tag.find('source')['src']
        }]
    else:
        compose = ['text']
        resource = None
    item['resource'] = resource
    item['compose'] = compose
    return item
