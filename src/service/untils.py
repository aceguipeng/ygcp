# -*- coding: utf-8 -*-
import os
import time
import traceback
import uuid
from distutils import dir_util
from random import randint, choice
from urllib.parse import urlparse

import requests

from common.const import CONST
from common.loggers.logger import log
from common.wrappers.redis_queue import RedisQueue
from service.globals import Globals
from settings.setting import SETTING


def check_ip_conn(ip):
    try:
        requests.get(CONST.CHECK_IP_URL)
        return True
    except Exception:
        log.error(traceback.format_exc())
        log.info('ip:{} connect failed'.format(ip))
        return False


def judge_url_source(url):
    dict = {
        'twitter.com': 'Twitter',
        'www.tumblr.com': 'Tumblr'
    }
    domain = url.split('/')[2]
    return dict[domain]


def save_resource_to_local(item):
    try:
        clear_cache_file_queue = RedisQueue('clear', Globals.get_redis_wrapper().get_redis_conn())
        timestamp = int(round(time.time() * 1000))
        resources = item.get('resource')
        for resource in resources:
            resource_addr = resource['addr']
            file_name = '{}_{}'.format(item['source_web'], resource_addr.split('/')[-1])
            log.info('try to get url:{}'.format(resource_addr))
            suffix_file = file_name.split('.')[-1]
            if suffix_file is None:
                file_name = '{}.mp4'.format(file_name)
            resp = requests.get(resource_addr, headers={'user-agent': choice(CONST.USER_AGENT_LIST)})
            file_data = resp.content
            parent_path = '{}/{}'.format(CONST.STATIC_FOLDER, resource['type'])
            if not os.path.exists(parent_path):
                dir_util.mkpath(parent_path)
            file_path = '{}/{}'.format(parent_path, file_name)
            with open(file_path, 'wb') as file:
                file.write(file_data)

            local_url = '{}/{}/{}'.format(SETTING.STATIC_URL, resource['type'], file_name)
            resource['local_url'] = local_url
            cache_file = {
                'file_name': file_path,
                'timestamp': timestamp
            }
            clear_cache_file_queue.put(cache_file)
        return item
    except Exception:
        log.error(traceback.format_exc())


def insert_items(items):
    try:
        mongo_db = Globals.get_mongo_wrapper().db
        collection = mongo_db['test_creepy_info']
        for item in items:
            resource = item.get('resource')
            if resource:
                item = save_resource_to_local(item)

            collection.insert_one(item)
    except Exception:
        log.error(traceback.format_exc())


def format_proxy(raw_proxy):
    proxy_type = raw_proxy.get(CONST.PROXY_TYPE)
    proxy_addr = raw_proxy.get(CONST.PROXY_ADDR)
    if (proxy_type is None) or (proxy_addr is None):
        return None

    # may don't have auth for this proxy
    proxy_auth = raw_proxy.get(CONST.PROXY_AUTH)
    if proxy_auth is None:
        proxy_str = '{}://{}'.format(proxy_type, proxy_addr)
    else:
        proxy_str = '{}://{}@{}'.format(proxy_type, proxy_auth, proxy_addr)

    valid_socks = ('socks5', 'socks4')
    if proxy_type not in valid_socks:
        proxy = {proxy_type: proxy_str}
    else:
        proxy = {'http': proxy_str, 'https': proxy_str}

    log.info('after formatting proxy: {}'.format(proxy))
    return proxy


def get_url_suffix(url):
    prefix_set = ['txt', 'doc', 'hlp', 'wps', 'rtf', 'pdf', 'ini',
                  'cfg', 'conf', 'ppt', 'xls', 'xlsx', 'xslt',
                  'rar', 'zip', 'arj', 'gz', 'z', '7z', 'tar',
                  'wav', 'au', 'aif', 'mp3', 'ram', 'wma', 'mmf', 'amr', 'aac', 'flac',
                  'avi', 'mpg', 'mov', 'swf', 'rm', 'mpeg',
                  'int', 'sys', 'dll', 'adt',
                  'exe', 'com',
                  'c', 'asm', 'for', 'lib', 'lst', 'msg', 'obj', 'pas', 'wki', 'bas',
                  'h', 'x', 'inc', 'java', 'py', 'php',
                  'map', 'iso',
                  'bak', 'dot',
                  'bat', 'sh',
                  'bmp', 'gif', 'jpg', 'jpeg', 'pic', 'png', 'tif']

    try:
        parse_uri = urlparse(url)
        path = parse_uri.path.strip()
        prefix_parts = path.split('.')
        prefix = prefix_parts[len(prefix_parts) - 1].lower()
        if prefix not in prefix_set:
            prefix = 'html'
    except Exception as err:
        log.error('get prefix from url error:{}'.format(str(err)))
        prefix = 'html'
        pass

    return prefix


def save_html_to_file(html, req_url):
    file_prefix = 'task_{}'.format(''.join(str(uuid.uuid4()).split('-')[:16]))
    full_file_prefix = os.path.join(CONST.STORAGE_PATH, file_prefix)
    file_suffix = get_url_suffix(req_url)
    full_file_name = '{}_{}_{}.{}'.format(full_file_prefix, randint(1000, 9999),
                                          int(time.time()), file_suffix)

    try:
        if not os.path.exists(CONST.STORAGE_PATH):
            dir_util.mkpath(CONST.STORAGE_PATH)
        f = open(full_file_name, 'wb+')
    except Exception as e:
        log.error('open file fail, task_id: {}, file: {}, url: {}, error: {}'.format(
            file_prefix, full_file_name, req_url, str(e)))
        return None

    try:
        f.write(html.encode())
    finally:
        f.close()

    return full_file_name
