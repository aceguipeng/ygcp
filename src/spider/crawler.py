# -*- coding: utf-8 -*-
import time
import uuid
from threading import Thread

from common.loggers.logger import log
from common.wrappers.redis_queue import RedisQueue
from settings.setting import SETTING
from spider.phantomjs import PhantomjsGet


class CrawlerManager:
    def __init__(self, task_queue, retry_times):
        self.task_queue = task_queue
        self.retry_times = retry_times
        self.thread_list = []
        self.thread_num = int(SETTING.MAX_THREAD_NUM)

    def release(self):
        try:
            for crawler_thread in self.thread_list:
                log.info('try to realse thread')
                crawler_thread.stop()
                self.thread_list.remove(crawler_thread)
        except Exception as e:
            log.error('release error=[{}]'.format(str(e)))

    def start(self):
        log.info('crawler manager try to start all threads,thread_num:{}'.format(self.thread_num))
        try:
            for i in range(self.thread_num):
                log.info('try to create thread:{}'.format(i + 1))
                crawler_thread = CrawlerThread(self.task_queue, self.retry_times)
                self.thread_list.append(crawler_thread)
                crawler_thread.start()
        except Exception as e:
            log.error('start error=[{}]'.format(str(e)))
            self.release()
            pass

    def repair(self):
        log.info('crawler manager try to repair')
        dead_thread_list = []
        try:
            for crawler_thread in self.thread_list:
                if not crawler_thread.is_alive():
                    dead_thread_list.append(crawler_thread)
            for c_thread in dead_thread_list:
                crawler_thread = CrawlerThread(self.task_queue, self.retry_times)
                self.thread_list.append(crawler_thread)
                crawler_thread.start()
                self.thread_list.remove(c_thread)
        except Exception as e:
            log.error('start error=[{}]'.format(str(e)))

    def get_status(self):
        log.info('crawler manager try to get status')
        num_of_alive = 0
        num_of_dead = 0

        for crawler_thread in self.thread_list:
            if crawler_thread.is_alive():
                num_of_alive += 1
            else:
                num_of_dead += 1
        return num_of_alive, num_of_dead

    def stop(self):
        log.info('crawler manager try to stop')
        self.release()


class CrawlerThread:
    def __init__(self, task_queue: RedisQueue, retry_times):
        self.is_running = False
        self.task_queue = task_queue
        self.retry_times = retry_times

    def crawl_file(self):
        while self.is_running:
            try:
                if self.task_queue.is_empty():
                    log.debug('task queue is empty...')
                    time.sleep(10)
                    continue
                try:
                    task = self.task_queue.get()
                    if task:
                        task = eval(task)
                except Exception as pass_error:
                    log.debug(str(pass_error))
                    time.sleep(1)
                    continue
                try:
                    phantomjs_crawl = PhantomjsGet()
                    phantomjs_crawl.get(task)
                except Exception as e:
                    err_msg = 'ERROR in get next task =[{}]'.format(str(e))
                    log.error(err_msg)
                    continue
                log.debug('spider task {} to request successfully.'.format(task['id']))
            except Exception as e:
                err_msg = 'ERROR in crawl_file =[{}]'.format(str(e))
                log.error(err_msg)
        return

    def start(self):
        log.info('crawl thread start')
        self.is_running = True
        Thread(target=self.crawl_file).start()
        return

    def stop(self):
        self.is_running = False
        log.info('crawler thread is go to stop')
        return

    def is_alive(self):
        return self.is_running


class Task(object):
    def __init__(self, url, raw_proxy, user_agent):
        self.url = url
        self.raw_proxy = raw_proxy
        self.user_agent = user_agent
        self.id = ''.join(str(uuid.uuid4()).split('-'))
