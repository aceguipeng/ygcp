# -*- coding: utf-8 -*-
import time

from selenium import webdriver
from selenium.webdriver import DesiredCapabilities
from selenium.webdriver.phantomjs.webdriver import WebDriver

from common.const import CONST
from common.loggers.logger import log
from common.wrappers.redis_queue import RedisQueue
from service.globals import Globals
from service.untils import save_html_to_file, judge_url_source
from settings.setting import SETTING


class PhantomjsGet(object):
    def __init__(self):
        self.phantomjs_path = SETTING.PHANTOMJS_PATH
        self.driver = None

    def get_page(self, url):
        log.info('PhantomjsGet start to crawl: {}'.format(url))
        self.driver.get(url)
        time.sleep(SETTING.DYNAMIC_WEB_TIMEOUT)
        count = 0
        while count < SETTING.SCROLL_TIMES:
            self.driver.execute_script("window.scrollTo(0,document.body.scrollHeight);")
            count += 1
            time.sleep(3)
        html = self.driver.page_source
        if len(html) <= 256:
            return None
        log.info('PhantomjsGet has completed crawl: {}'.format(url))
        file_name = save_html_to_file(html, url)
        # 放队列
        source = judge_url_source(url)
        timestamp = int(round(time.time() * 1000))
        item_task = {
            'source_web': source,
            'file_name': file_name,
            'timestamp': timestamp
        }
        process_queue = RedisQueue('process_items', Globals.get_redis_wrapper().get_redis_conn())
        clear_cache_file_queue = RedisQueue('clear', Globals.get_redis_wrapper().get_redis_conn())
        process_queue.put(item_task)
        clear_cache_file_queue.put(item_task)
        return file_name

    def get(self, task):
        try:
            raw_proxy = task.get('raw_proxy')
            user_agent = task.get('user_agent')
            url = task.get('url')
            driver = self._get_web_driver(raw_proxy, user_agent)
        except Exception as e:
            raise Exception('PhantomjsGet get driver error: {}'.format(str(e)))
        try:
            self.driver = driver
            file_name = self.get_page(url)
        except Exception as e:
            raise Exception('PhantomjsGet download error: {}'.format(str(e)))
        finally:
            driver.quit()

        return file_name

    def _get_web_driver(self, raw_proxy, user_agent) -> WebDriver:
        # set user-agent arguments.
        dcap = self.dcap_with_ua(user_agent)
        if raw_proxy:
            # set proxy arguments.
            service_args = self._get_service_args(raw_proxy)
        else:
            service_args = None

        try:
            driver = webdriver.PhantomJS(self.phantomjs_path, desired_capabilities=dcap,
                                         service_args=service_args)
        except Exception as e:
            log.error('init JSMiddleware fail, error={}'.format(str(e)))
            raise e
        return driver

    def dcap_with_ua(self, user_agent):
        dcap = DesiredCapabilities.PHANTOMJS.copy()
        dcap["phantomjs.page.settings.userAgent"] = user_agent
        return dcap

    @staticmethod
    def _get_service_args(raw_proxy):
        service_args = ['--proxy={}'.format(raw_proxy[CONST.PROXY_ADDR]),
                        '--proxy-type={}'.format(raw_proxy[CONST.PROXY_TYPE])]
        proxy_auth = raw_proxy.get(CONST.PROXY_AUTH)
        if proxy_auth is not None:
            service_args.append('--proxy-auth={}'.format(proxy_auth))
        return service_args
