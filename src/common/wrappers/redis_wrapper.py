# -*-coding:utf-8-*-

import redis
from redis import Redis


class RedisWrapper(object):
    def __init__(self, host, port, password=None):
        if password is None and password == '':
            redis_pool = redis.ConnectionPool(host=host, port=port)
        else:
            redis_pool = redis.ConnectionPool(host=host, port=port, password=password)
        self.redis_conn = redis.Redis(connection_pool=redis_pool)

    def get_redis_conn(self) -> Redis:
        return self.redis_conn
