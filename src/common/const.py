class _CONST(object):
    def __setattr__(self, *_):
        raise SyntaxError('Trying to change a constant value')

    SYSTEM_NAME = 'ygcp'
    SUBSYSTEM_NAME = 'web'

    MAX_BACK_FILE_NUM = 10
    MAX_BACK_FILE_SIZE = 256 * 1024 * 1024

    # PROJECTNAME
    PROJECT_NAME = 'ygcp_web'

    # encoding
    CODE_UTF8 = 'utf-8'
    UTF_8 = 'utf8'
    PARSER_LXML = 'lxml'
    FILE_HTML = 'html'

    CRAWLER_DEFAULT = 'crawler_default'
    CRAWLER_PHANTOMJS = 'crawler_phantomjs'
    CRAWLER_FIREFOX = 'crawler_firefox'

    PROXY_TYPE = 'type'
    PROXY_ADDR = 'addr'
    PROXY_AUTH = 'auth'

    TASK_TYPE = 'type'
    TASK_URL = 'url'
    TASK_ID = 'task_id'
    DYNAMIC = 'dynamic'
    METHOD = 'method'
    RESOURCE_TYPE = 'resource_type'
    REQUEST_DATA = 'request_data'
    HEADER = 'header'
    STATUS = 'status'
    VPS_GROUP_ID = 'vps_group_id'
    ERROR_REASON = 'error_reason'
    RESULT = 'result'

    SUCCESS = 'SUCCESS'
    FAILURE = 'FAIL'

    MAX_ROBOT_CHECK_RETRIES = 10

    METHOD_GET = 'GET'
    METHOD_POST = 'POST'
    METHOD_PUT = 'PUT'
    METHOD_DELETE = 'DELETE'
    CONTENT_TYPE = 'Content-Type'

    VALID_METHOD_SET = [METHOD_POST, METHOD_GET, METHOD_PUT, METHOD_DELETE]

    DATA_TYPE_JSON = 'application/json'
    DATA_TYPE_POST_FORM = 'application/x-www-form-urlencoded'

    STORAGE_PATH = '../data/htmls'
    APP_STATIC_FOLDER = '../../data/uploads'
    STATIC_FOLDER = '../data/uploads'
    STATIC_URL = '/ygcp/web/api/v1/static'

    URL_LIST = (
        'https://www.tumblr.com/search/FOOD', 'https://www.tumblr.com/search/ANIMATION',
        'https://www.tumblr.com/search/BOOK',
        'https://www.tumblr.com/search/SPORT', 'https://www.tumblr.com/search/CAMPUS',
        'https://www.tumblr.com/search/CAMPUS',
        'https://www.tumblr.com/search/CAREER', 'https://www.tumblr.com/search/TV+SHOW',
        'https://www.tumblr.com/search/TECHNOLOGY',
        'https://www.tumblr.com/search/MUSIC',
        'https://www.tumblr.com/search/MOVIE',
        'https://www.tumblr.com/search/AUTO',
        'https://www.tumblr.com/search/GAME',
        'https://twitter.com/search?src=type&q=LIFESTYLE',
        'https://twitter.com/search?src=type&q=FOOD',
        'https://twitter.com/search?src=type&q=LIFESTYLE',
        'https://twitter.com/search?src=type&q=PHOTOGRAPHY',
        'https://twitter.com/search?src=type&q=SPORT', 'https://twitter.com/search?src=type&q=BOOK',
        'https://twitter.com/search?src=type&q=FUNNY',
        'https://twitter.com/search?src=type&q=CAMPUS', 'https://twitter.com/search?src=type&q=TV%20SHOW',
        'https://twitter.com/search?src=type&q=CAREER',
        'https://twitter.com/search?src=type&q=MUSIC',
        'https://twitter.com/search?src=type&q=MOVIE',
        'https://twitter.com/search?src=type&q=AUTO',
        'https://twitter.com/search?src=type&q=OTHER',)

    USER_AGENT_LIST = (
        'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.8) Gecko Fedora/1.9.0.8-1.fc10 Kazehakase/0.5.6',
        'Mozilla/5.0 (X11; Linux i686; U;) Gecko/20070322 Kazehakase/0.4.5',
        'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/21.0.1180.71 Safari/537.1 LBBROWSER',
        'Opera/9.25 (Windows NT 5.1; U; en)',
        'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322; .NET CLR 2.0.50727)',
        'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.56 Safari/535.11',
        'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.64 Safari/537.11',
        'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; QQDownload 732; .NET4.0C; .NET4.0E)',
        'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/535.11 (KHTML, like Gecko) Chrome/17.0.963.56 Safari/535.11',
        'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 5.1; Trident/4.0; SV1; QQDownload 732; .NET4.0C; .NET4.0E; 360SE)',
        'Mozilla/4.0 (compatible; MSIE 7.0b; Windows NT 5.2; .NET CLR 1.1.4322; .NET CLR 2.0.50727; InfoPath.2; .NET CLR 3.0.04506.30)',
        'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_7_3) AppleWebKit/535.20 (KHTML, like Gecko) Chrome/19.0.1036.7 Safari/535.20',
        'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.9.0.8) Gecko Fedora/1.9.0.8-1.fc10 Kazehakase/0.5.6',
        'Mozilla/5.0 (X11; U; Linux x86_64; zh-CN; rv:1.9.2.10) Gecko/20100922 Ubuntu/10.10 (maverick) Firefox/3.6.10',
        'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/21.0.1180.71 Safari/537.1 LBBROWSER',
        'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/21.0.1180.89 Safari/537.1',
        'Mozilla/4.0 (compatible; MSIE 7.0; Windows NT 6.0; Acoo Browser; SLCC1; .NET CLR 2.0.50727; Media Center PC 5.0; .NET CLR 3.0.04506)',
        'Mozilla/5.0 (X11; U; Linux i686; en-US; rv:1.8.0.12) Gecko/20070731 Ubuntu/dapper-security Firefox/1.5.0.12',
        'Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; QQDownload 732; .NET4.0C; .NET4.0E; LBBROWSER)',
        'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/21.0.1180.89 Safari/537.1',
        'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.100 Safari/537.36',
        'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:56.0) Gecko/20100101 Firefox/56.0',
    )


CONST = _CONST()
