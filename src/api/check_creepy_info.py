# -*- coding:utf-8 -*-
import traceback

from jsonschema import validate

from common.loggers.logger import log

SORTED_WAY = 'sorted_way'
PAGE_NO = 'page_no'
PAGE_SIZE = 'page_size'
SORTED_TYPE = 'sorted_type'
SUBJECT = 'subject'
SOURCE_WEB = 'source_web'
ID = 'id'
UPDATE_TIME = 'update_time'
HEAT = 'heat'
DATE = 'date'


def check_get_api(dict_data):
    schema = {
        'required': [SORTED_WAY, SORTED_TYPE, PAGE_NO, PAGE_SIZE],
        'additionalProperties': False,
        'type': 'object',
        'properties': {
            SORTED_WAY: {
                'description': '排序方式',
                'type': 'string',
                'enum': [
                    '1',
                    '-1'
                ]
            },
            SORTED_TYPE: {
                'description': '排序依据',
                'type': 'string',
                'enum': [
                    'update_time',
                    'heat'
                ]
            },
            UPDATE_TIME: {
                'description': '更新时间,筛选依据',
                'type': 'string',
            },
            PAGE_NO: {
                'description': '当前页',
                'type': 'string'
            },
            PAGE_SIZE: {
                'description': '页面显示数据个数',
                'type': 'string'
            },
            SUBJECT: {
                'description': '话题，筛选依据',
                'type': 'string'
            },
            SOURCE_WEB: {
                'description': '来源网站，筛选依据',
                'type': 'string'
            },
            ID: {
                'description': '主键',
                'type': 'string'
            },
            DATE: {
                'description': '日期区间',
                'type': 'string',
                'enum': ['1', '3', '7', '15', '30']
            }
        }
    }
    try:
        validate(dict_data, schema)
        return {'result': 'success'}
    except Exception as e:
        log.info(traceback.format_exc())
        return {'error_code': 56001, 'reason': str(e), 'result': 'failure'}
