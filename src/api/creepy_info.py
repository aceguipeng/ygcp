# -*- coding:utf-8 -*-
from flask import request
from flask_restful import Resource

from api.check_creepy_info import check_get_api
from service.mongo_service.creepy_service import select_creepy_info, select_one_creepy


class CreepyInfo(Resource):
    def get(self):
        dict_data = request.args.to_dict()
        check = check_get_api(dict_data)
        if check['result'] != 'success':
            return check
        if 'id' in dict_data:
            result = select_one_creepy(dict_data)
        else:
            sorted_way = int(dict_data['sorted_way'])
            sorted_type = dict_data.get('sorted_type')
            result = select_creepy_info(dict_data, sort=sorted_type, sorted_way=sorted_way)
        return result
