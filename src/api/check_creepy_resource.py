# -*- coding:utf-8 -*-
import traceback

from jsonschema import validate

from common.loggers.logger import log

RESOURCE = 'resource'
ID = 'id'
LOCAL_URL = 'local_url'


def check_post_api(dict_data):
    schema = {
        'required': [RESOURCE, ID],
        'additionalProperties': False,
        'type': 'object',
        'properties': {
            RESOURCE: {
                'description': '本地链接',
                'type': 'array',
                'items': {
                    'required': [LOCAL_URL],
                    'additionalProperties': False,
                    'type': 'object',
                    'properties': {
                        LOCAL_URL: {
                            'description': '本地url',
                            'type': 'string',
                        }
                    }
                }
            },
            ID: {
                'description': '所属信息id',
                'type': 'string',
            },
        }
    }
    try:
        validate(dict_data, schema)
        return {'result': 'success'}
    except Exception as e:
        log.info(traceback.format_exc())
        return {'error_code': 56001, 'reason': str(e), 'result': 'failure'}
