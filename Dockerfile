#python3 development environment
FROM registry.cn-hangzhou.aliyuncs.com/yiguo/ubuntu_python:v3
MAINTAINER pi "guipeng8789502@163.com"

WORKDIR /src
ADD ./src /src

RUN pip3 install -r requirements.txt

ENV ETCD_SERVICE_SERVICE_HOST=192.168.0.203
ENV ETCD_SERVICE_SERVICE_PORT=22379

EXPOSE 5002

CMD ["python3.5", "app.py"]